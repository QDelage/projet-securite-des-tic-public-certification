#!/usr/bin/python3
from bottle import route, run, template, request, response
import datetime
import subprocess
import stegano
from PIL import Image, ImageDraw, ImageFilter, ImageFont
import qrcode
from pyzbar.pyzbar import decode
import time



@route('/certificat', method='POST')
def creation_certificat():
    """
    Crée et retourne un certificat

    :POST : identite: nom de la personne
    :POST : certif: nom de la certification
    """
    identite = request.forms.get('identite')
    certif = request.forms.get('certif')

    # On se crée un dossier pour la requete en cours

    folder = '/tmp/certifplus/%s'%str(time.time())

    subprocess.run('mkdir -p %s'%folder, shell = True)

    f = open('%s/certifplus.txt'%folder, 'w')

    f.write(identite + ';' + certif + ';' + str(datetime.datetime.now()))
    f.close()

    # On signe les informations avec la clef du serveur
    cmd = 'openssl dgst -sign AC\ Keys/ecc.key.pem -keyform PEM -sha256 -out %s/certifplus.txt.sign -binary %s/certifplus.txt'%(folder, folder)
    
    subprocess.run(cmd, shell = True)

    # On crée la requête du timestamp
    cmd_timestamp_request = 'openssl ts -query -data %s/certifplus.txt.sign -no_nonce -sha512 -cert -out %s/certifplus.tsq'%(folder, folder)

    subprocess.run(cmd_timestamp_request, shell = True)

    # On effectue la requête et on stocke le certificat
    cmd_timestamp_request = 'curl -H "Content-Type: application/timestamp-query" --data-binary "@%s/certifplus.tsq" https://freetsa.org/tsr > %s/certifplus.tsr'%(folder, folder)

    subprocess.run(cmd_timestamp_request, shell = True)

    response.set_header('Content-type', 'image/png')

    img = Image.open('fond_attestation.png')

    # On a besoin de passer le certifcat en base64 pour pouvoir le cacher

    subprocess.run('openssl enc -base64 -in %s/certifplus.tsr -out %s/certifplus.tsr.b64'%(folder, folder), shell = True)

    f = open('%s/certifplus.tsr.b64'%folder, 'r')

    estampille = f.read()
    f.close()

    img.save('%s/certifplus_attestation_en_cours.png'%folder)

    # On crée puis ajoute le code QR, puis le texte
    creer_QR_code(folder)

    concatQR_image(folder)

    ecrire_texte(identite, certif, folder)

    # Enfin, on renvoie le certificat au format PNG au client

    response.set_header('Content-type', 'image/png')
    descripteur_fichier = open('%s/certifplus_attestation_en_cours.png'%folder,'rb')
    
    img = Image.open('%s/certifplus_attestation_en_cours.png'%folder)
    # On cache le certificat de timestamp dans l'image

    f = open('%s/certifplus.txt'%folder, 'r')
    infos = f.read()
    f.close()

    a_cacher = infos + ';' + estampille
    a_cacher = a_cacher.ljust(8000, ';')

    stegano.cacher(img, a_cacher)

    img.save('%s/certifplus_attestation_en_cours.png'%folder)

    contenu_fichier = descripteur_fichier.read()
    descripteur_fichier.close()

    # Enfin, on peut supprimer le dossier de travail
    subprocess.run('rm -rf %s'%folder, shell = True);

    return contenu_fichier

def creer_QR_code(folder):
    """
    Crée le code QR correspondant au certificat signé (d'abord convertit en base 64)
    :param folder: Le dossier où on travaille
    """

    subprocess.run('openssl enc -base64 -in %s/certifplus.txt.sign -out %s/certifplus.txt.sign.b64'%(folder, folder), shell = True)

    f = open('%s/certifplus.txt.sign.b64'%folder, 'r')
    data = f.read()
    f.close()
    
    qr = qrcode.QRCode(version=1, box_size=10, border=4)
    qr.add_data(data)
    qr.make()
    img = qr.make_image()

    img = img.resize((80, 80))
    img.save('%s/certifplus_QR_en_cours.png'%folder, quality=20,optimize=True)


def concatQR_image(folder):
    """
    Ajoute le code QR sur l'image
    :param folder: Le dossier où on travaille
    """
    img = Image.open('%s/certifplus_attestation_en_cours.png'%folder)
    qr = Image.open('%s/certifplus_QR_en_cours.png'%folder)

    img.paste(qr,(1480, 1000))
    img.save("%s/certifplus_attestation_en_cours.png"%folder)


def ecrire_texte(identite, certif, folder):
    """
    Appose le texte sur l'image

    :param identite: Nom de la personne
    :param certif: Nom de la certification
    :param folder: Le dossier où on travaille
    """
    img = Image.open('%s/certifplus_attestation_en_cours.png'%folder)
    x, y = img.size

    imd = ImageDraw.Draw(img)    
    ma_font = ImageFont.truetype("LiberationMono-Regular.ttf", 70)

    txt1 = 'Certificat de ' + certif
    txt2 = 'délivré à'
    txt3 = identite
    x1, y1 = imd.textsize(txt1, font = ma_font)
    x2, y2 = imd.textsize(txt2, font = ma_font)
    x3, y3 = imd.textsize(txt3, font = ma_font)

    imd.text(((x - x1) / 2, ((y - y1) / 2) - y2), txt1, fill = (0, 0, 0), font = ma_font)
    imd.text(((x - x2) / 2, ((y - y2) / 2)), txt2, fill = (0, 0, 0), font = ma_font)
    imd.text(((x - x3) / 2, ((y - y3) / 2) + y2), txt3, fill = (0, 0, 0), font = ma_font)

    img.save('%s/certifplus_attestation_en_cours.png'%folder)

@route('/verification', method='POST')
def verification_attestation():
    contenu_image = request.files.get('image')
 
    # On se crée un dossier pour la requete en cours
    folder = '/tmp/certifplus/%s'%str(time.time())

    subprocess.run('mkdir -p %s'%folder, shell = True)

    contenu_image.save('%s/attestation_a_verifier.png'%folder, overwrite=True)

    # On récupère les infos et le timestamp
    msg = stegano.recuperer(Image.open('%s/attestation_a_verifier.png'%folder), 8000)
 
    # On écrit les infos
    f = open('%s/infos.txt'%folder, 'w')
    if len(msg.split(';')) >= 3:
        f.write(msg.split(';')[0] + ';' + msg.split(';')[1] + ';' + msg.split(';')[2])
    else:
        f.write(' ')
    f.close()

    # On écrit le timestamp
    f = open('%s/timestamp.b64'%folder, 'w')
    if len(msg.split(';')) >= 3:
        f.write(msg.split(';')[3])
    else:
        f.write(' ')
    f.close()

    # On décode le timestamp
    subprocess.run('openssl base64 -d -in %s/timestamp.b64 -out %s/timestamp.tsr'%(folder, folder), shell = True)


    # On récupère la signature dans le QR Code

    decodedQR = decode(Image.open('%s/attestation_a_verifier.png'%folder))

    f = open('%s/signature.b64'%folder, 'wb')
    if decodedQR:
        f.write(decodedQR[0].data)
    f.close()

    # On décode la signature

    subprocess.run('openssl base64 -d -in %s/signature.b64 -out %s/signature'%(folder, folder), shell = True)

    # On vérifie la signature

    signature = subprocess.run('openssl dgst -verify "AC Keys/pub.pem" -keyform PEM -sha256 -signature %s/signature -binary %s/infos.txt'%(folder, folder), shell = True)

    if signature.returncode != 0:
        return "attestation erronée \n"

    # On recrée la requete de timestamp
    subprocess.run('openssl ts -query -data %s/signature -no_nonce -sha512 -cert -out %s/file.tsq'%(folder, folder), shell = True)

    # Enfin, on vérifie le timestamp

    verif = subprocess.run('openssl ts -verify -in %s/timestamp.tsr -queryfile %s/file.tsq -CAfile freetsa/cacert.pem -untrusted freetsa/tsa.crt'%(folder, folder), shell = True)

    # Enfin, on peut supprimer le dossier de travail
    subprocess.run('rm -rf %s'%folder, shell = True);


    if verif.returncode == 0:
        return "certifié\n"
    return "attestation erronée"


run(host='0.0.0.0',port=8080,debug=True)
