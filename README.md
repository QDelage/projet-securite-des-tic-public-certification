# Project certificate authority - public

Public repository of one of my first-year of Master degree projects about the coding of a certificate authority, realised in pair (anonymised).

<img src="screenshots/false_certificate.png" width="500">

## Getting Started

### Prerequisites

What things you need to install the software and how to install them

You will need these Python3 libraries:

* argparse
* PIL
* qrcode
* pyzbar.decode

You will also need openssl and curl

### Usage

First, run the server, then use the client for requests.

On this example, we use a proxy to secure the connexion with the client using SSL (all the certificates are in the folder 'AC Keys', but expired now.

First:

```
python server.py
```

On another tty:

```
socat \
openssl-listen:9000,fork,reuseaddr,cert=AC\ Keys/bundle_serveur.pem,cafile=AC\ Keys/ecc.ca.cert.pem,verify=0 \
tcp:127.0.0.1:8080
```

Then on another tty, run the requests:
```
python client.py --help
```

![Usage screenshot](screenshots/Example.png "Usage screenshot")

## Built With

* [Bottle: Python Web Framework](https://bottlepy.org/docs/dev/) - The web framework used
* [OpenSSL](https://www.openssl.org/) - Cryptography library

## Authors

* **Quentin DELAGE** - *Initial work* - [QDelage](https://gitlab.com/QDelage)
* **Pair** - *Initial work* - anonymised

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

