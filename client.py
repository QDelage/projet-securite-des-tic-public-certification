import subprocess
import argparse
import sys

parser = argparse.ArgumentParser(description='Client for CertifPlus')

parser.add_argument('--file', '-f', dest='file', default="certificate.png", type=str, help='Path to the image file to created / verified. Default is "certificate.png"')
parser.add_argument('--name', '-n', dest='name', default="John Smith", type=str, help='Your name. Default is "John Smith"')
parser.add_argument('--certif', '-c', dest='certif', default="Cryptis", type=str, help='The title of your certificate. Default is "Cryptis"')
parser.add_argument('action', nargs='*', help='Action to perform. One of these : "create", or "verify"')

args = parser.parse_args()

if len(args.action) != 1:
    parser.print_help()
    sys.exit(1)

# Processus de création
if args.action[0] == "create":
    subprocess.run("curl -X POST -d 'identite=%s' -d 'certif=%s' https://localhost:9000/certificat -o %s --cacert AC\ Keys/ecc.ca.cert.pem"%(args.name, args.certif, args.file), shell = True)

# Processus de vérification
elif args.action[0] == "verify":
    verify = subprocess.Popen("curl -v -F image=@%s https://localhost:9000/verification --cacert AC\ Keys/ecc.ca.cert.pem"%args.file, shell = True, stdout=subprocess.PIPE, stderr=subprocess.PIPE) 
    

    # On vérifie la réponse retournée par le serveur
    # (Attention à l'accent)
    if b"certifi\xc3\xa9" in  verify.stdout.read():
        print("Le certificat est valide.")
    else:
        print("Le certificat n'est pas valide.")
